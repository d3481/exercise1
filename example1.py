# Autor: Gabriel Elias Paredes Mendoza
# -- coding: utf-8 --
import statistics
def input_clean(inp):
    try:
        if inp[-1]==' ':
            inp=inp[:-1]
        return inp
    except:
        operation = input('Ingreso de valores no permitidos\n 1. Volver a ejecutar\n 2. Cerrar\n')
        if operation=="2":
            quit()
def get_operators():
    operators = input('Ingrese operadores separado por espacios:\n')
    operatorsList=[]
    if len(operators)==0:
        print('No se ha ingresado operadores')
    operators = input_clean(operators)
    try:
        operatorsList = [int(x) for x in operators.split(' ')]
    except:
        print('Los operadores ingresados no son números')
    return operatorsList
def operationSelector(operation):
    operation=operation.upper()
    if operation in ['1','SUMA']:
        operatorsList = get_operators()
        result= sum(operatorsList)
        print(f'Resultado: {result}')
    elif operation in['2','PROMEDIO']:
        operatorsList=get_operators()
        result=statistics.mean(operatorsList)
        print(f'Resultado: {result}')
    else:
        print(f'No existe la operación : {operation}')
        operation=input_clean('')
    input('Presione enter para continuar')
    
if __name__ == "__main__":
    print('Bienvenido a la prueba técnica de Duodata')
    while 1:
        operation=input('\nOperaciones disponibles  \n 1. Suma\n 2. Promedio\nIngrese la operación a realizar:\n')
        operation=input_clean(operation)
        operationSelector(operation)